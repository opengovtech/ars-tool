#!/bin/bash
set -e

./download_geojson.sh
poetry install
poetry run python convert_vz250.py
poetry run python convert_vg250.py