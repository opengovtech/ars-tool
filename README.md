<!--
SPDX-FileCopyrightText: 2021 codedust

SPDX-License-Identifier: EUPL-1.2
-->

# ARS-Tool

Simple tool to explore [Amtliche Regionalschlüssel (ARS)](https://www.destatis.de/DE/Themen/Laender-Regionen/Regionales/Gemeindeverzeichnis/_inhalt.html) of German regions.
A live version of this tool can be found [here](https://www.opengovtech.de/ars/).

## Update geojson shapes
First, install `poetry` and install python dependencies using `poetry`:

```console
$ poetry install
```

Next, download geojson data and update `web/ars_from_geojson.json` as well as all geojson files in `web/geojson/` by executing the `update.sh` script:

```console
$ ./update.sh
```

## Serving static content
The content of the 'web/' directory can now be served statically.

```console
$ cd web
$ python -m http.server
```

## License
Licensed under the [EUPL](./LICENSE.txt).
