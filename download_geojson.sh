#!/bin/bash

# see available WFS services: https://geoportal.de/search.html?q=verwaltungszuordnungen&filter.resourcetype=Geodatendienst&filter.service=Downloaddienst

EPSG_CRS='4326' # coordinate reference system based on the Earth's center of mass, used by the GPS among others (default coordinate system of leaflet)

# download Verwaltungszuordnungen (different ARS levels)
echo vz250 is deprecated. This should now be included in table vgtb_vz_gem within vg250.
#SERVICE_NAME='wfs_vz250_1231'
#FEATURE_TYPE='vz250_1231:vz250'
#DOWNLOAD_URL="https://sgx.geodatenzentrum.de/$SERVICE_NAME?service=WFS&request=GetFeature&version=2.0.0&typeNames=$FEATURE_TYPE&outputFormat=application%2Fjson&srsName=urn:ogc:def:crs:EPSG::$EPSG_CRS&resultType=results"
#echo $DOWNLOAD_URL
#wget --show-progress -O "data/${SERVICE_NAME}_epsg_${EPSG_CRS}.geojson" $DOWNLOAD_URL


# download geojson shapes for all ars
SERVICE_NAME='wfs_vg250'

# see https://sg.geodatenzentrum.de/web_public/gdz/dokumentation/deu/vg250.pdf
declare -a FEATURE_TYPES=("vg250_sta" "vg250_lan" "vg250_rbz" "vg250_krs" "vg250_vwg" "vg250_gem")

for FEATURE_TYPE in "${FEATURE_TYPES[@]}"
do
    DOWNLOAD_URL="https://sgx.geodatenzentrum.de/$SERVICE_NAME?service=WFS&request=GetFeature&version=2.0.0&typeNames=$FEATURE_TYPE&outputFormat=application%2Fjson&srsName=urn:ogc:def:crs:EPSG::$EPSG_CRS&resultType=results"
    echo $DOWNLOAD_URL
    wget --show-progress -O "data/${SERVICE_NAME}_${FEATURE_TYPE}_epsg_${EPSG_CRS}.geojson" $DOWNLOAD_URL
done