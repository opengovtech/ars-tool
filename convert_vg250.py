import copy
import json
import sys

# create geosjon shapes for all ARSs
# VG = Verwaltungsgebiete

GEOJSON_TEMPLATE = {
  "type": "FeatureCollection",
  "features": []
}

file_names = [
    'data/wfs_vg250_vg250_sta_epsg_4326.geojson',
    'data/wfs_vg250_vg250_lan_epsg_4326.geojson',
    'data/wfs_vg250_vg250_rbz_epsg_4326.geojson',
    'data/wfs_vg250_vg250_krs_epsg_4326.geojson',
    'data/wfs_vg250_vg250_vwg_epsg_4326.geojson',
    'data/wfs_vg250_vg250_gem_epsg_4326.geojson',
]

for file_name in file_names:
    with open(file_name) as file:
        data = json.load(file)
        for feature in data['features']:
            # create geojson file
            if feature['properties']['gf'] != 4: # GF: Geofaktor; "Zur ausschließlichen Darstellung ohne die Gebiete auf Nord- oder Ostsee bzw. Bodensee ist auf GF = 4 zu filtern."
                continue

            # write geojson file for each ars
            geojson = copy.deepcopy(GEOJSON_TEMPLATE)
            geojson['features'] = [feature]

            ars = feature['properties']['ars']
            filename = f'web/geojson/{ars}.geojson.json'

            with open(filename, 'w') as f:
                print(f"Writing {filename}")
                f.write(json.dumps(geojson))